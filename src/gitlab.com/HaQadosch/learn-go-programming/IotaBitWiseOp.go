package main

import (
	"fmt"
)

// Enumerated constant: when you just want the constants to be of different values, but don't care about the values per se.
// to be used like flags or labels.
const (
	a = iota // the const block reproduce the pattern of the first assignment.
	b
	c
)

// iota scope per const block.
// Restart at 0.
const (
	i = iota
	f
	g
)

// iota start as the zero value and might end up being equeal to any non initialised const.
// to make the enumerated const start at 1...
const (
	_ = iota // we skip the zero value altogether
	const1
	const2
	const3
)

// bit flags example
const (
	isAdmin          = 1 << iota // 001
	isHeadquarters               // 010
	canSeeFinancials             // 100

	canSeeAfrica
	canSeeASia
	canSeeEurope
	canSeeAmerica
)

func constantOp() {
	fmt.Printf("%v, %T\n", a, a)
	fmt.Printf("%v, %T\n", c, c)
	fmt.Printf("%v, %T\n", b, b)
	fmt.Printf("%v, %T\n", i, i)
	fmt.Printf("%v, %T\n", f, f)
	fmt.Printf("%v, %T\n", g, g)
	fmt.Printf("%v, %T\n", const1, const1)
	fmt.Printf("%v, %T\n", const2, const2)
	fmt.Printf("%v, %T\n", const3, const3)

	// with the bit flag you can define roles if at least one of the flags below is true.
	var roles byte = isAdmin | canSeeFinancials | canSeeEurope
	fmt.Printf("%v %b\n", roles, roles)
	fmt.Printf("isAdmin?\t%v\n", isAdmin&roles == isAdmin) // bitMask and. Only the bits set in both isAdmin and Roles will remain.
	fmt.Printf("canSeeAmerica?\t%v\n", canSeeAmerica&roles == canSeeAmerica)

}
