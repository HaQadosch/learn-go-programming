package main

import (
	"bytes"
	"fmt"
	"strconv"
)

func main() {
	var w Writer = ConsoleWriter{}

	StuffCounted := IntCount(0) // We need to cast the int into a IntCount.
	var inc Incrementer = &StuffCounted
	for i := 0; i < 10; i++ {
		w.Write([]byte(strconv.Itoa(inc.Increment())))
	}

	var wc WriterCloser = &NewBufferedWriterCloser()
	wc.BWrite([]byte("Hello this is an experiment, please disregard any hasard."))
	wc.Close()

	// wc as a WriterCloser is not aware of the implementation details.
	// So it cannot access any buffer internally.
	// By casting it as a BufferedWriterCloser, I imply that now there is a buffer.
	// Wicth means that bwc can now access that buffer and manipulate it.
	bwc, ok := wc.(*BufferedWriterCloser)
	if ok {
		fmt.Println(bwc)
	} else {
		fmt.Println("Conversion failed")
	}

	var MyObj interface{} = NewBufferedWriterCloser() // an empty interface accepts everything.
	if myWc, err := MyObj.(WriterCloser); ok {
		myWc.Write([]byte("This is a test"))
		myWc.Close()
	} else {
		fmt.Println("Conversion failed")
	}
}

/*
	Writer
*/

// Writer ...
type Writer interface {
	Write([]byte) (int, error)
}

// ConsoleWriter ...
type ConsoleWriter struct{}

// Write implements the interface Writer
func (cw ConsoleWriter) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data))
	return n, err
}

/*
	Incrementer
*/

// Incrementer ...
type Incrementer interface {
	Increment() int
}

// IntCount is an alias for int.
type IntCount int

// Increment ...
func (inc *IntCount) Increment() int {
	*inc++
	return int(*inc)
}

/*
	Writer Closer
*/

// Interface Writer implemented higher in the page.

// BWriter ..
type BWriter interface {
	BWrite([]byte) (int, error)
}

// Closer ...
type Closer interface {
	Close() error
}

// WriterCloser ...
type WriterCloser interface {
	BWriter
	Closer
}

// BufferedWriterCloser ...
type BufferedWriterCloser struct {
	buffer *bytes.Buffer
}

// BWrite ...
func (bwc *BufferedWriterCloser) BWrite(data []byte) (int, error) {
	n, err := bwc.buffer.Write(data)
	if err != nil {
		return 0, err
	}

	v := make([]byte, 8)
	for bwc.buffer.Len() > 8 {
		if _, err := bwc.buffer.Read(v); err != nil {
			return 0, err
		}

		if _, err = fmt.Println(string(v)); err != nil {
			return 0, err
		}
	}

	return n, nil
}

// Close ...
func (bwc *BufferedWriterCloser) Close() error {
	for bwc.buffer.Len() > 0 {
		data := bwc.buffer.Next(8)
		if _, err := fmt.Println(string(data)); err != nil {
			return err
		}
	}
	return nil
}

// NewBufferedWriterCloser ...
func NewBufferedWriterCloser() *BufferedWriterCloser {
	return &BufferedWriterCloser{
		buffer: bytes.NewBuffer([]byte{}),
	}
}
