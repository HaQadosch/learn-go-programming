package main

import (
	"fmt"
)

func arraySlice() {
	grades := [...]int{97, 85, 79}
	fmt.Printf("%v %d %d\n", grades, len(grades), cap(grades))

	identity := [3][3]int{[3]int{1, 0, 0}, [3]int{0, 1, 0}, [3]int{0, 0, 1}}
	id := identity // copy by value.
	fmt.Printf("%v %p\n", identity, &identity)
	fmt.Printf("%v %p\n", id, &id)
	id[0][0] = 5
	fmt.Printf("%v\n%v\n", identity, id)

	slc := []int{1, 2, 3} // slice
	sld := slc            // copy by ref
	fmt.Printf("%v %p %v %v\n", slc, &slc, len(slc), cap(slc))
	fmt.Printf("%v %p %v %v\n", sld, &sld, len(sld), cap(sld))
	sld[0] = 5
	fmt.Printf("%v\n%v\n", slc, sld)

	// create slice from an array. [include:exclude]
	a := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	b := a[:]   // sliced over all of a
	c := a[3:]  // start the slice at 4th elt of a
	d := a[:4]  // slice cover a up to the 5th elt.
	e := a[3:6] // cover a from 4th up to 7th
	fmt.Println("a", a, len(a), cap(a))
	fmt.Println("b", b, len(b), cap(b))
	fmt.Println("c", c, len(c), cap(c))
	fmt.Println("d", d, len(d), cap(d))
	fmt.Println("e", e, len(e), cap(e))

	// all the slices are pointing to the same array
	a[0] = 11
	fmt.Println("a", a, len(a), cap(a))
	fmt.Println("b", b, len(b), cap(b))
	fmt.Println("c", c, len(c), cap(c))
	fmt.Println("d", d, len(d), cap(d))
	fmt.Println("e", e, len(e), cap(e))

	// create slice or array using make function make(type, len, cap)
	f := []int{}
	fmt.Println("f", f, len(f), cap(f))
	f = append(f, 10, 20, 30, 40)
	fmt.Println("f", f, len(f), cap(f))
	f = append(f, 50)
	fmt.Println("f", f, len(f), cap(f))

	// spread operator is at the end of the range value
	g := []int{11, 12, 13}
	h := append(f, g...)
	fmt.Println("h", h, len(h), cap(h))
}
