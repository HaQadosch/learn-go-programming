package main

import "fmt"

func main() {
	board := map[string]string{
		"president": "Josiah Hall",
		"finance":   "Wiley Pember",
		"admin":     "Elmer Papin",
	}

	for k, v := range board {
		fmt.Println(k, v)
	}
}
