package main

import (
	"fmt"
	"reflect"
)

func main() {
	// MAPS
	// Since keys have to be compared to each other, the types must have a equality operator defined: primitives, array, channels, but not slices.

	// all the keys have the same type, all the values have the same type.
	board := map[string]string{
		"president": "Josiah Hall",
		"finance":   "Wiley Pember",
		"admin":     "Elmer Papin",
	}
	fmt.Printf("%v\n", board)

	members := make(map[string]string, 2)
	fmt.Printf("%v\n", members)
	members["first"] = "Ted Lawrence"
	fmt.Printf("%v\n", members)

	// Like slices, map has an underlying structure, and all is done by ref.
	m := members
	m["first"] = "Golda Antunes"
	fmt.Printf("%v\t%v\n", members, m)

	members["second"] = "Ted Lawrence"
	fmt.Printf("%v\n", members)

	fmt.Printf("%v\n", members["joker"]) // doesn't raise an error. return the 0 value of the type if not found.
	value, ok := members["joker"]        // using the , ok "comma ok" syntax
	if !ok {
		fmt.Printf("%v not found\n", "joker")
	} else {
		fmt.Printf("%v, %v\n", value, ok)
	}

	// struct can be declared as a type.

	type someone struct {
		name     string
		birthday string
		age      uint
		friends  []someone
	}

	lynn := someone{
		name:     "Lynn Reshid",
		birthday: "1962-10-14",
		age:      58,
		friends:  []someone{},
	}
	jina := someone{
		name:     "Jina Bonardi",
		birthday: "1947-09-18",
		age:      73,
		friends:  []someone{lynn},
	}
	lynn.friends = append(lynn.friends, jina)
	fmt.Printf("%v, friends: %v\n", lynn, lynn.friends)

	// they can also be anonymous and used once and immediately
	brianne := struct{ name string }{name: "Brianne Shaikh"}
	fmt.Println(brianne)

	// Pass by value
	edmond := brianne // it's a copy not a pointer
	edmond.name = "Edmond Rostand"
	fmt.Println(brianne, edmond)

	// Embedding as composition, no inheritance
	type animal struct {
		name   string `max:"5"`
		origin string
	}

	type bird struct {
		animal // embedding
		canFly bool
		speed  float64
	}

	florentino := bird{}
	florentino.name = "Florentino"
	florentino.origin = "flat 22"
	florentino.canFly = true
	florentino.speed = 154

	fmt.Println(florentino)

	t := reflect.TypeOf(animal{})
	field, _ := t.FieldByName("name")
	fmt.Println(field.Tag)
}
