package main

import "fmt"

func main() {
	i := 10
	switch {
	case i <= 10:
		fmt.Println("case 1")
		fallthrough // force the run of the next case no matter what
	case i <= 20:
		fmt.Println("case 2")
	default:
		fmt.Println("default")
	}

	// Type switch using interface
	var j interface{} = [3]int{}
	switch j.(type) {
	case int:
		fmt.Println("int")
	case float64:
		fmt.Println("float")
	case [3]int:
		fmt.Println("[3]int")
		fmt.Println("this will print")
		break
		fmt.Println("this will NOT print")
	default:
		fmt.Println("is another type")
	}
}
